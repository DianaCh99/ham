window.addEventListener("DOMContentLoaded", () => {
  /* service tabs */

  $(document).ready(function () {
    const titles = document.querySelectorAll(".tabs__title");
    const contents = document.querySelectorAll(".tabs__content");

    titles.forEach((title, index) => {

      title.addEventListener("click", () => {
        titles.forEach(title => title.classList.remove("tabs__title-active"));
        contents.forEach(content => content.classList.remove("tabs__content-active"));
        title.classList.add("tabs__title-active");
        contents[index].classList.add("tabs__content-active");
      });

    });
  })
  /*Work filter*/

  /* Cards flip, Create back-side for each card */

  function setupFilterCards(imgSelected) {
    $(imgSelected).each(function () {
      $(this).wrap("<div class='filter__card'></div>");
      $(this).parent().append("<div class='filter__card__back'></div>");
      $(this).wrap("<div class='filter__card__front'></div>");
      $(".filter__card__back").html('<div class="back__icon-container"><div class="back__icon1"></div><div class="back__icon2"></div></div> <h2 class="back__title">creative design</h2><p class="back__descr">Web Design</p>');
      $(".filter__card__back").hide();
    });

    $(".filter__card").each(function () {
      const $front = $(this).find(".filter__card__front");
      const $back = $(this).find(".filter__card__back");

      $(this).hover(function () {
        $front.hide(100);
        $back.show(100);
      }, function () {
        $front.show(100);
        $back.hide(100);
      });
    });
  }
  setupFilterCards(".filter__img")

  /* filter images */

  $(".filter__menu-item").on("click", function () {
    const category = $(this).data("category");

    // Remove filter cards and new-img
    $('.filter__card').each(function () {
      if ($(this).has('.new-img').length) {
        $(this).remove();
      }
    });

    $(".filter__btn").show();

    $(".filter__menu-item").removeClass("active");
    $(this).addClass("active");
    $(".filter_images.active").removeClass("active");
    $(".filter_images." + category).addClass("active");

    displayedImagesCount = 0;
  });

  /* list of new pictures */

  const imageList = [
    //all
    { src: './img/wordpress/wordpress7.jpg', category: 'all' },
    { src: './img/all/all2.jpg', category: 'all' },
    { src: './img/all/all3.jpg', category: 'all' },
    { src: './img/all/all4.jpg', category: 'all' },
    { src: './img/all/all5.jpg', category: 'all' },
    { src: './img/all/all6.jpg', category: 'all' },
    { src: './img/all/all7.jpg', category: 'all' },
    { src: './img/wordpress/wordpress8.jpg', category: 'all' },
    { src: './img/all/all9.jpg', category: 'all' },
    { src: './img/all/all10.jpg', category: 'all' },
    { src: './img/all/all11.jpg', category: 'all' },
    { src: './img/graphic-design/graphic-design20.jpg', category: 'all' },
    { src: './img/all/all2.jpg', category: 'all' },
    { src: './img/graphic-design/graphic-design24.jpg', category: 'all' },
    { src: './img/web-design/web-design1.jpg', category: 'all' },
    { src: './img/wordpress/wordpress9.jpg', category: 'all' },
    { src: './img/all/all5.jpg', category: 'all' },
    { src: './img/all/all6.jpg', category: 'all' },
    { src: './img/wordpress/wordpress14.jpg', category: 'all' },
    { src: './img/all/all4.jpg', category: 'all' },
    { src: './img/all/all7.jpg', category: 'all' },
    { src: './img/graphic-design/graphic-design23.jpg', category: 'all' },
    { src: './img/wordpress/wordpress12.jpg', category: 'all' },
    { src: './img/graphic-design/graphic-design13.jpg', category: 'all' },
    //graphic-design
    { src: './img/graphic-design/graphic-design13.jpg', category: 'graphic-design' },
    { src: './img/graphic-design/graphic-design14.jpg', category: 'graphic-design' },
    { src: './img/graphic-design/graphic-design15.jpg', category: 'graphic-design' },
    { src: './img/graphic-design/graphic-design16.jpg', category: 'graphic-design' },
    { src: './img/graphic-design/graphic-design17.jpg', category: 'graphic-design' },
    { src: './img/graphic-design/graphic-design18.jpg', category: 'graphic-design' },
    { src: './img/graphic-design/graphic-design19.jpg', category: 'graphic-design' },
    { src: './img/graphic-design/graphic-design20.jpg', category: 'graphic-design' },
    { src: './img/graphic-design/graphic-design21.jpg', category: 'graphic-design' },
    { src: './img/graphic-design/graphic-design22.jpg', category: 'graphic-design' },
    { src: './img/graphic-design/graphic-design23.jpg', category: 'graphic-design' },
    { src: './img/graphic-design/graphic-design24.jpg', category: 'graphic-design' },
    //web-design
    { src: './img/web-design/web-design1.jpg', category: 'web-design' },
    { src: './img/web-design/web-design2.jpg', category: 'web-design' },
    { src: './img/web-design/web-design3.jpg', category: 'web-design' },
    { src: './img/web-design/web-design4.jpg', category: 'web-design' },
    { src: './img/web-design/web-design5.jpg', category: 'web-design' },
    { src: './img/web-design/web-design6.jpg', category: 'web-design' },
    { src: './img/web-design/web-design7.jpg', category: 'web-design' },
    { src: './img/web-design/web-design8.jpg', category: 'web-design' },
    { src: './img/web-design/web-design9.jpg', category: 'web-design' },
    { src: './img/web-design/web-design10.jpg', category: 'web-design' },
    { src: './img/web-design/web-design11.jpg', category: 'web-design' },
    { src: './img/web-design/web-design12.jpg', category: 'web-design' },
    { src: './img/web-design/web-design13.jpg', category: 'web-design' },
    { src: './img/web-design/web-design14.jpg', category: 'web-design' },
    { src: './img/web-design/web-design15.jpg', category: 'web-design' },
    //landing-pages
    { src: './img/landing-page/landing-page1.jpg', category: 'landing-pages' },
    { src: './img/landing-page/landing-page2.jpg', category: 'landing-pages' },
    { src: './img/landing-page/landing-page3.jpg', category: 'landing-pages' },
    { src: './img/landing-page/landing-page4.jpg', category: 'landing-pages' },
    { src: './img/landing-page/landing-page5.jpg', category: 'landing-pages' },
    { src: './img/landing-page/landing-page6.jpg', category: 'landing-pages' },
    { src: './img/landing-page/landing-page7.jpg', category: 'landing-pages' },
    { src: './img/landing-page/landing-page8.jpg', category: 'landing-pages' },
    { src: './img/landing-page/landing-page9.jpg', category: 'landing-pages' },
    { src: './img/landing-page/landing-page10.jpg', category: 'landing-pages' },
    { src: './img/landing-page/landing-page11.jpg', category: 'landing-pages' },

    //wordpress
    { src: './img/wordpress/wordpress1.jpg', category: 'wordpress' },
    { src: './img/wordpress/wordpress2.jpg', category: 'wordpress' },
    { src: './img/wordpress/wordpress3.jpg', category: 'wordpress' },
    { src: './img/wordpress/wordpress4.jpg', category: 'wordpress' },
    { src: './img/wordpress/wordpress5.jpg', category: 'wordpress' },
    { src: './img/wordpress/wordpress6.jpg', category: 'wordpress' },
    { src: './img/wordpress/wordpress7.jpg', category: 'wordpress' },
    { src: './img/wordpress/wordpress8.jpg', category: 'wordpress' },
    { src: './img/wordpress/wordpress9.jpg', category: 'wordpress' },
    { src: './img/wordpress/wordpress10.jpg', category: 'wordpress' },
    { src: './img/wordpress/wordpress11.jpg', category: 'wordpress' },
    { src: './img/wordpress/wordpress12.jpg', category: 'wordpress' },
    { src: './img/wordpress/wordpress13.jpg', category: 'wordpress' },
    { src: './img/wordpress/wordpress14.jpg', category: 'wordpress' },
    { src: './img/wordpress/wordpress5.jpg', category: 'wordpress' },
    { src: './img/wordpress/wordpress6.jpg', category: 'wordpress' },
    { src: './img/wordpress/wordpress7.jpg', category: 'wordpress' },
    { src: './img/wordpress/wordpress8.jpg', category: 'wordpress' },
    { src: './img/wordpress/wordpress9.jpg', category: 'wordpress' },
    { src: './img/wordpress/wordpress10.jpg', category: 'wordpress' },
  ];

  let displayedImagesCount = 0;

  function addImageToFilter(filterImages, image) {
    const img = document.createElement('img');
    img.src = image.src;
    img.alt = image.category;
    img.classList.add("new-img");
    filterImages.append(img);
  }

  function updateFilterButton(remainingImagesCount) {
    if (remainingImagesCount <= 0 || displayedImagesCount >= 24) {
      $(".filter__btn").hide();
    }
  }

  function addImagesByCategory(imageList, selectedCategory) {
    const filterImages = document.querySelector(`.filter_images.${selectedCategory}`);
    const imagesToAdd = imageList.filter(image => image.category === selectedCategory).slice(displayedImagesCount, displayedImagesCount + 12);
    displayedImagesCount += imagesToAdd.length;

    imagesToAdd.forEach(image => {
      addImageToFilter(filterImages, image);
    });

    const remainingImagesCount = imageList.filter(image => image.category === selectedCategory).length - displayedImagesCount;
    setupFilterCards(".new-img");
    updateFilterButton(remainingImagesCount);
  }

  $(".filter__btn").on("click", function () {
    const selectedCategory = $(".filter__menu-item.active").data('category');

    $(".filter__btn").addClass('loading');

    setTimeout(function () {
      addImagesByCategory(imageList, selectedCategory);
      $(".filter__btn").removeClass('loading');
    }, 1500);
  });

  /* feedback slider-slick */

  $(document).ready(function () {
    const sliderFor = $('.slider-for');
    const sliderNav = $('.slider-nav');

    sliderFor.slick({
      slidesToShow: 1,
      fade: true,
      asNavFor: sliderNav,
      draggable: true,
      arrows: false,
    });

    sliderNav.slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: sliderFor,
      centerMode: true,
      focusOnSelect: true,
      arrows: true,
      prevArrow: "<button class='slick-prev'>&#10094;</button>",
      nextArrow: "<button class='slick-next'>&#10095;</button>",
    }).find('.slick-track').css('margin-top', '26px');
  })


  /* gallery grid masonry */

  $(document).ready(function () {

    const galleryGrid = $('.gallery__grid').masonry({
      itemSelector: '.gallery__item',
      gutter: 12,
      originBottom: false,
    });

    const galleryImages = [
      './img/gallary of images/img10.jpg',
      './img/gallary of images/img11.jpg',
      './img/gallary of images/img12.jpg',
      './img/gallary of images/img13.jpg',
      './img/gallary of images/img14.jpg',
      './img/gallary of images/img15.jpg',
      './img/gallary of images/img16.jpg',
      './img/gallary of images/img17.jpg',
      './img/gallary of images/img18.jpg',
      './img/gallary of images/img19.jpg',
      './img/gallary of images/img20.jpg',
      './img/gallary of images/img21.jpg',
      './img/gallary of images/img22.jpg',
      './img/gallary of images/img23.jpg',
      './img/gallary of images/img24.jpg',
      './img/gallary of images/img25.jpg',
    ];


    const loadBtn = $('.gallery__btn');
    let index = 0;

    loadBtn.on('click', function () {
      loadBtn.addClass('loading');
      $('.icon-plus').hide();

      for (let i = 0; i < 6; i++) {

        const img = new Image();
        
        img.onload = function () {

          setTimeout(function () {
            galleryGrid.append(img).masonry('appended', img);
            loadBtn.removeClass('loading');
            $('.icon-plus').show();
            if (index >= galleryImages.length) {
              loadBtn.hide();
              return;
            }
          }, 1500);

        };

        img.src = galleryImages[index];
        img.alt = 'design';
        $(img).addClass('gallery__item');

        index++;
      }
    });
  })
})








